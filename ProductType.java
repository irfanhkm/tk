package Core;

import java.util.List;

public class ProductType {

    /**
        Code Smell Type: Data classes
        Solution: Ganti tipe properti variabel ke public dan hapus function getter setter
    **/
    private String productTypeCode;
    private String productTypeName;

    public String getProductTypeCode() {
        return productTypeCode;
    }

    public void setProductTypeCode(String productTypeCode) {
        this.productTypeCode = productTypeCode;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public List<ProductType> select()
    {
        return new DataSource().selectProductTypeFromDB();
    }

    public void insert()
    {
        new DataSource().insertProductTypeToDB(this);
    }

    public void update()
    {
        new DataSource().updateProductTypeToDB(this);
    }

    public void remove()
    {
        new DataSource().removeProductTypeFromDB(this);
    }
}
