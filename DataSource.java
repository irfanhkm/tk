package Core;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
/**
   Code Smell Type: Dead Code
   Solution: Hapus line kode
**/
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;

public class DataSource {

    private Connection conn;
    private Statement stmt;
    private ResultSet rs;

    public List<Customer> selectCustomerFromDB()
    {
        List<Customer> customer = new Vector<Customer>();
        try
        {
            /**
                Code smell type: Duplicate Code
                Solution: pindah ke function sendiri atau bikin class sendiri utk koneksi database
            **/
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "select * from customer";

            rs = stmt.executeQuery(q);

            while(rs.next())
            {
                Customer s = new Customer();
                s.setCustomerCode(rs.getString("customercode"));
                s.setCustomerName(rs.getString("customername"));
                s.setAddress(rs.getString("address"));

                customer.add(s);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:selectCustomerFromDB] : "+ e);
        }
        return customer;
    }

    public void insertCustomerToDB(Customer customer)
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "insert into customer values('"+customer.getCustomerCode()+"','"+customer.getCustomerName()+"','"+customer.getAddress()+"')";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:insertCustomerToDB] : "+ e);
        }
    }

    public void updateCustomerToDB(Customer customer)
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "update customer set customername = '"+customer.getCustomerName()+"', address = '"+customer.getAddress()+"' where customercode = '"+customer.getCustomerCode()+"'";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:updateCustomerToDB] : "+ e);
        }
    }

    public void removeCustomerFromDB(Customer customer)
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "delete from customer where customercode = '"+customer.getCustomerCode()+"'";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:removeCustomerFromDB] : "+ e);
        }
    }

    public List<Product> selectProductFromDB()
    {
        List<Product> product = new Vector<Product>();
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "select * from product";

            rs = stmt.executeQuery(q);

            while(rs.next())
            {
                Product s = new Product();
                s.setProductCode(rs.getString("productcode"));
                s.setProductName(rs.getString("productname"));
                s.setStock(rs.getInt("stock"));
                s.setPrice(rs.getInt("price"));

                List<ProductType> productType = this.selectProductTypeFromDB();

                for(int i=0;i<productType.size();i++)
                {
                    if(productType.get(i).getProductTypeCode().equals(rs.getString("producttypecode")))
                        s.setProductType(productType.get(i));
                }

                product.add(s);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:selectProductFromDB] : "+ e);
        }
        return product;
    }

    public void insertProductToDB(Product product)
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "insert into product values('"+product.getProductCode()+"','"+product.getProductName()+"','"+product.getProductType().getProductTypeCode()+"',"+product.getPrice()+","+product.getStock()+")";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:insertProductToDB] : "+ e);
        }
    }

    public void updateProductToDB(Product product)
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "update product set productname = '"+product.getProductName()+"', producttypecode = '"+product.getProductType().getProductTypeCode()+"', price = "+product.getPrice()+", stock = "+product.getStock()+" where productcode = '"+product.getProductCode()+"'";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:updateProductToDB] : "+ e);
        }
    }

    public void removeProductFromDB(Product product)
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "delete from product where productcode = '"+product.getProductCode()+"'";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:removeProductFromDB] : "+ e);
        }
    }

    public List<ProductType> selectProductTypeFromDB()
    {
        List<ProductType> productType = new Vector<ProductType>();
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "select * from producttype";

            rs = stmt.executeQuery(q);

            while(rs.next())
            {
                ProductType s = new ProductType();
                s.setProductTypeCode(rs.getString("producttypecode"));
                s.setProductTypeName(rs.getString("producttypename"));

                productType.add(s);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:selectProductTypeFromDB] : "+ e);
        }
        return productType;
    }

    public void insertProductTypeToDB(ProductType productType)
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "insert into producttype values('"+productType.getProductTypeCode()+"','"+productType.getProductTypeName()+"')";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:insertProductTypeToDB] : "+ e);
        }
    }

    public void updateProductTypeToDB(ProductType productType)
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "update producttype set producttypename = '"+productType.getProductTypeName()+"' where producttypecode = '"+productType.getProductTypeCode()+"'";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:updateProductTypeToDB] : "+ e);
        }
    }

    public void removeProductTypeFromDB(ProductType productType)
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "delete producttype where producttypecode = '"+productType.getProductTypeCode()+"'";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:removeProductTypeFromDB] : "+ e);
        }
    }

    public List<Transaction> selectTransactionFromDB()
    {
        List<Transaction> transaction = new Vector<Transaction>();
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "select * from transaction";

            rs = stmt.executeQuery(q);

            while(rs.next())
            {
                Transaction s = new Transaction();
                s.setTransactionNumber(rs.getString("transactionnumber"));
                s.setTransactionDate(rs.getDate("transactiondate"));
                s.setItem(this.selectDetailTransactionFromDB(rs.getString("transactionnumber")));

                List<Customer> customer = this.selectCustomerFromDB();
                
                for(int i=0;i<customer.size();i++)
                {
                    if(customer.get(i).getCustomerCode().equals(rs.getString("customercode")))
                        s.setCustomer(customer.get(i));
                }
                
                transaction.add(s);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:selectTransactionFromDB] : "+ e);
        }
        return transaction;
    }

    public List<Transaction.DetailTransaction> selectDetailTransactionFromDB(String transactionNumber)
    {
        List<Transaction.DetailTransaction> detailTransaction = new Vector<Transaction.DetailTransaction>();
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "select * from detailtransaction where transactionnumber = '"+transactionNumber+"'";

            rs = stmt.executeQuery(q);

            while(rs.next())
            {
                Transaction.DetailTransaction s = new Transaction.DetailTransaction();
                s.setPrice(rs.getInt("price"));
                s.setQty(rs.getInt("qty"));

                List<Product> product = this.selectProductFromDB();
                
                for(int i=0;i<product.size();i++)
                {
                    if(product.get(i).getProductCode().equals(rs.getString("productcode")))
                        s.setProduct(product.get(i));
                }

                detailTransaction.add(s);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:selectDetailTransactionFromDB] : "+ e);
        }
        return detailTransaction;
    }

    public void insertTransactionToDB(Transaction transaction)
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "insert into transaction values ('"+transaction.getTransactionNumber()+"','"+transaction.getTransactionDate()+"','"+transaction.getCustomer().getCustomerCode()+"')";

            stmt.executeUpdate(q);

            for(int i=0;i<transaction.getItem().size();i++)
            {
                q = "insert into detailtransaction values ('"+transaction.getTransactionNumber()+"','"+transaction.getItem().get(i).getProduct().getProductCode()+"',"+transaction.getItem().get(i).getQty()+","+transaction.getItem().get(i).getPrice()+")";

                stmt.executeUpdate(q);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:insertTransactionToDB] : "+ e);
        }
    }

    public void updateTransactionToDB(Transaction transaction)
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);

            String q = "update transaction set transactiondate = '"+transaction.getTransactionDate()+"', customercode = '"+transaction.getCustomer().getCustomerCode()+"' where transactionnumber = '"+transaction.getTransactionNumber()+"'";

            stmt.executeUpdate(q);

            q = "delete from detailtransaction where transactionnumber = '"+transaction.getTransactionNumber()+"'";

            stmt.executeUpdate(q);

            for(int i=0;i<transaction.getItem().size();i++)
            {
                q = "insert into detailtransaction values ('"+transaction.getTransactionNumber()+"','"+transaction.getItem().get(i).getProduct().getProductCode()+"',"+transaction.getItem().get(i).getQty()+","+transaction.getItem().get(i).getPrice()+")";

                stmt.executeUpdate(q);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:updateTransactionToDB] : "+ e);
        }
    }

    public void removeTransactionFromDB(Transaction transaction)
    {
        try
        {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
            stmt = conn.createStatement(1004,1008);
            
            String q = "delete from detailtransaction where transactionnumber = '"+transaction.getTransactionNumber()+"'";

            stmt.executeUpdate(q);

            q = "delete from transaction where transactionnumber = '"+transaction.getTransactionNumber()+"'";

            stmt.executeUpdate(q);

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:removeTransactionFromDB] : "+ e);
        }
    }
}
