package Core;

import java.util.List;

public class Customer {

    public String customerCode;
    public String customerName;
    public String address;

    public List<Customer> select()
    {
        return new DataSource().selectCustomerFromDB();
    }

    public void insert()
    {
        new DataSource().insertCustomerToDB(this);
    }

    public void update()
    {
        new DataSource().updateCustomerToDB(this);
    }

    public void remove()
    {
        new DataSource().removeCustomerFromDB(this);
    }
}
