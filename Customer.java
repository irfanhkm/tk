package Core;

import java.util.List;

public class Customer {

    /**
        Code Smell Type: Data classes
        Solution: Ganti tipe properti variabel ke public dan hapus function getter setter
    **/
    private String customerCode;
    private String customerName;
    private String address;

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Customer> select()
    {
        return new DataSource().selectCustomerFromDB();
    }

    public void insert()
    {
        new DataSource().insertCustomerToDB(this);
    }

    public void update()
    {
        new DataSource().updateCustomerToDB(this);
    }

    public void remove()
    {
        new DataSource().removeCustomerFromDB(this);
    }
}
