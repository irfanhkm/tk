package Core;

import java.util.List;

public class Product {

    public String productCode;
    public String productName;
    public int stock;
    public int price;
    public ProductType productType;

    public List<Product> select()
    {
        return new DataSource().selectProductFromDB();
    }

    public void insert()
    {
        new DataSource().insertProductToDB(this);
    }

    public void update()
    {
        new DataSource().updateProductToDB(this);
    }

    public void remove()
    {
        new DataSource().removeProductFromDB(this);
    }
}

