package Core;

import java.util.List;

public class Product {

    /**
        Code Smell Type: Data classes
        Solution: Ganti tipe properti variabel ke public dan hapus function getter setter
    **/
    private String productCode;
    private String productName;
    private int stock;
    private int price;
    private ProductType productType;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public List<Product> select()
    {
        return new DataSource().selectProductFromDB();
    }

    public void insert()
    {
        new DataSource().insertProductToDB(this);
    }

    public void update()
    {
        new DataSource().updateProductToDB(this);
    }

    public void remove()
    {
        new DataSource().removeProductFromDB(this);
    }
}
