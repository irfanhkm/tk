package Core;

import java.util.Date;
import java.util.List;
import java.util.Vector;

public class Transaction {

    /**
        Code Smell Type: Data classes
        Solution: Ganti tipe properti variabel ke public dan hapus function getter setter
    **/
    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<DetailTransaction> getItem() {
        return item;
    }

    public void setItem(List<DetailTransaction> item) {
        this.item = item;
    }

    /**
        Code smell type: Large Class Type
        Solution: extract class
    **/
    public static class DetailTransaction
    {
        /**
            Code Smell Type: Data classes
            Solution: Ganti tipe properti variabel ke public dan hapus function getter setter
        **/
        public Product product;
        public int qty;
        public int price;
    }

    /**
        Code Smell Type: Data classes
        Solution: Ganti tipe properti variabel ke public dan hapus function getter setter
    **/
    private String transactionNumber;
    private Date transactionDate;
    private Customer customer;
    private List<DetailTransaction> item = new Vector<DetailTransaction>();

    public List<Transaction> select()
    {
        return new DataSource().selectTransactionFromDB();
    }

    public void insert()
    {
        new DataSource().insertTransactionToDB(this);
    }

    public void update()
    {
        new DataSource().updateTransactionToDB(this);
    }

    public void remove()
    {
        new DataSource().removeTransactionFromDB(this);
    }
}
