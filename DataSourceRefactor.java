package Core;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;

public class DataSource {

    private Connection conn;
    private Statement stmt;
    private ResultSet rs;

    public DataSource()
    {
        this.setConnection();
    }

    private void setConnection()
    {
        Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
        conn = DriverManager.getConnection("jdbc:odbc:driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\Users\\user\\Documents\\NetBeansProjects\\ATSE2\\app_data\\data.mdb");
        stmt = conn.createStatement(1004,1008);
    }

    public List<Customer> selectCustomerFromDB()
    {
        List<Customer> customer = new Vector<Customer>();
        try
        {
            String q = "select * from customer";
            rs = stmt.executeQuery(q);

            while(rs.next())
            {
                Customer s = new Customer();
                s.customerCode = rs.getString("customercode");
                s.customerName = rs.getString("customername");
                s.address = rs.getString("address");

                customer.add(s);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:selectCustomerFromDB] : "+ e);
        }
        return customer;
    }

    public void insertCustomerToDB(Customer customer)
    {
        try
        {
            String q = "insert into customer values('"+customer.customerCode+"','"+customer.customerName+"','"+customer.address+"')";
            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:insertCustomerToDB] : "+ e);
        }
    }

    public void updateCustomerToDB(Customer customer)
    {
        try
        {
            String q = "update customer set customername = '"+customer.customerName+"', address = '"+customer.address+"' where customercode = '"+customer.customerCode+"'";
            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:updateCustomerToDB] : "+ e);
        }
    }

    public void removeCustomerFromDB(Customer customer)
    {
        try
        {
            String q = "delete from customer where customercode = '"+customer.customerCode+"'";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:removeCustomerFromDB] : "+ e);
        }
    }

    public List<Product> selectProductFromDB()
    {
        List<Product> product = new Vector<Product>();
        try
        {
            String q = "select * from product";
            rs = stmt.executeQuery(q);

            while(rs.next())
            {
                Product s = new Product();
                s.productCode = rs.getString("productcode");
                s.productName = rs.getString("productname");
                s.stock = rs.getInt("stock");
                s.price = rs.getInt("price");

                List<ProductType> productType = this.selectProductTypeFromDB();

                for(int i=0;i<productType.size();i++)
                {
                    if(productType.get(i).productTypeCode.equals(rs.getString("producttypecode")))
                        s.productType = productType.get(i);
                }

                product.add(s);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:selectProductFromDB] : "+ e);
        }
        return product;
    }

    public void insertProductToDB(Product product)
    {
        try
        {
            String q = "insert into product values('"+product.productCode+"','"+product.productName+"','"+product.productType.productTypeCode+"',"+product.price+","+product.stock+")";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:insertProductToDB] : "+ e);
        }
    }

    public void updateProductToDB(Product product)
    {
        try
        {
            String q = "update product set productname = '"+product.productName+"', producttypecode = '"+product.productType.productTypeCode+"', price = "+product.price+", stock = "+product.stock+" where productcode = '"+product.productCode+"'";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:updateProductToDB] : "+ e);
        }
    }

    public void removeProductFromDB(Product product)
    {
        try
        {
            String q = "delete from product where productcode = '"+product.productCode+"'";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:removeProductFromDB] : "+ e);
        }
    }

    public List<ProductType> selectProductTypeFromDB()
    {
        List<ProductType> productType = new Vector<ProductType>();
        try
        {
            String q = "select * from producttype";

            rs = stmt.executeQuery(q);

            while(rs.next())
            {
                ProductType s = new ProductType();
                s.productTypeCode = rs.getString("producttypecode");
                s.productTypeName = rs.getString("producttypename");

                productType.add(s);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:selectProductTypeFromDB] : "+ e);
        }
        return productType;
    }

    public void insertProductTypeToDB(ProductType productType)
    {
        try
        {
            String q = "insert into producttype values('"+productType.productTypeCode+"','"+productType.productTypeName+"')";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:insertProductTypeToDB] : "+ e);
        }
    }

    public void updateProductTypeToDB(ProductType productType)
    {
        try
        {
            String q = "update producttype set producttypename = '"+productType.productTypeName+"' where producttypecode = '"+productType.productTypeCode+"'";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:updateProductTypeToDB] : "+ e);
        }
    }

    public void removeProductTypeFromDB(ProductType productType)
    {
        try
        {
            String q = "delete producttype where producttypecode = '"+productType.productTypeCode+"'";

            stmt.executeUpdate(q);
            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:removeProductTypeFromDB] : "+ e);
        }
    }

    public List<Transaction> selectTransactionFromDB()
    {
        List<Transaction> transaction = new Vector<Transaction>();
        try
        {
            String q = "select * from transaction";
            rs = stmt.executeQuery(q);

            while(rs.next())
            {
                Transaction s = new Transaction();
                s.transactionNumber = rs.getString("transactionnumber");
                s.transactionDate = rs.getDate("transactiondate");
                s.item = this.selectDetailTransactionFromDB(rs.getString("transactionnumber"));

                List<Customer> customer = this.selectCustomerFromDB();
                
                for(int i=0;i<customer.size();i++)
                {
                    if(customer.get(i).customerCode.equals(rs.getString("customercode")))
                        s.customer = customer.get(i);
                }
                
                transaction.add(s);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:selectTransactionFromDB] : "+ e);
        }
        return transaction;
    }

    public List<Transaction.DetailTransaction> selectDetailTransactionFromDB(String transactionNumber)
    {
        List<Transaction.DetailTransaction> detailTransaction = new Vector<Transaction.DetailTransaction>();
        try
        {
            String q = "select * from detailtransaction where transactionnumber = '"+transactionNumber+"'";
            rs = stmt.executeQuery(q);

            while(rs.next())
            {
                Transaction.DetailTransaction s = new Transaction.DetailTransaction();
                s.price = rs.getInt("price");
                s.qty = rs.getInt("qty");

                List<Product> product = this.selectProductFromDB();
                
                for(int i=0;i<product.size();i++)
                {
                    if(product.get(i).productCode.equals(rs.getString("productcode")))
                        s.product = product.get(i);
                }

                detailTransaction.add(s);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:selectDetailTransactionFromDB] : "+ e);
        }
        return detailTransaction;
    }

    public void insertTransactionToDB(Transaction transaction)
    {
        try
        {
            String q = "insert into transaction values ('"+transaction.transactionNumber+"','"+transaction.transactionDate+"','"+transaction.customer.customerCode+"')";
            stmt.executeUpdate(q);

            for(int i=0;i<transaction.item.size();i++)
            {
                q = "insert into detailtransaction values ('"+transaction.transactionNumber+"','"+transaction.item.get(i).product.productCode+"',"+transaction.item.get(i).qty+","+transaction.item.get(i).price+")";

                stmt.executeUpdate(q);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:insertTransactionToDB] : "+ e);
        }
    }

    public void updateTransactionToDB(Transaction transaction)
    {
        try
        {
            String q = "update transaction set transactiondate = '"+transaction.transactionDate+"', customercode = '"+transaction.customer().customerCode+"' where transactionnumber = '"+transaction.transactionNumber+"'";
            stmt.executeUpdate(q);

            q = "delete from detailtransaction where transactionnumber = '"+transaction.transactionNumber+"'";
            stmt.executeUpdate(q);

            for(int i=0;i<transaction.item.size();i++)
            {
                q = "insert into detailtransaction values ('"+transaction.transactionNumber+"','"+transaction.item.get(i).product.productCode+"',"+transaction.item.get(i).qty+","+transaction.item.get(i).price+")";
                stmt.executeUpdate(q);
            }

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:updateTransactionToDB] : "+ e);
        }
    }

    public void removeTransactionFromDB(Transaction transaction)
    {
        try
        {
            String q = "delete from detailtransaction where transactionnumber = '"+transaction.transactionNumber+"'";
            stmt.executeUpdate(q);

            q = "delete from transaction where transactionnumber = '"+transaction.transactionNumber+"'";
            stmt.executeUpdate(q);

            conn.close();
        }catch(Exception e)
        {
            System.out.println("[DataSource:removeTransactionFromDB] : "+ e);
        }
    }
}
