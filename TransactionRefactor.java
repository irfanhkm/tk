package Core;

import java.util.Date;
import java.util.List;
import java.util.Vector;

public class Transaction {
    public String transactionNumber;
    public Date transactionDate;
    public Customer customer;
    public List<DetailTransaction> item = new Vector<DetailTransaction>();

    public List<Transaction> select()
    {
        return new DataSource().selectTransactionFromDB();
    }

    public void insert()
    {
        new DataSource().insertTransactionToDB(this);
    }

    public void update()
    {
        new DataSource().updateTransactionToDB(this);
    }

    public void remove()
    {
        new DataSource().removeTransactionFromDB(this);
    }
}
