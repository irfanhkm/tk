package Core;

import java.util.List;

public class ProductType {

    public String productTypeCode;
    public String productTypeName;

    public List<ProductType> select()
    {
        return new DataSource().selectProductTypeFromDB();
    }

    public void insert()
    {
        new DataSource().insertProductTypeToDB(this);
    }

    public void update()
    {
        new DataSource().updateProductTypeToDB(this);
    }

    public void remove()
    {
        new DataSource().removeProductTypeFromDB(this);
    }
}
